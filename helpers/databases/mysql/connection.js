const { isEqual } = require('lodash')
const { createPool } = require('mysql2')

const connectionPool = []

const getConnection = (config) => {
  const pool = connectionPool.find(pool => isEqual(pool.config, config))
  if (pool) {
    return pool
  }

  const newPool = { config, connection: createPool(config) }
  connectionPool.push(newPool)
  return newPool
}

const initMysqlPool = (config) => {
  getConnection(config)
}

module.exports = {
  initMysqlPool,
  getConnection,
}