// import _ from 'lodash'
const pool = require('./connection')

// type QueryStatementField = null | string | number | boolean | Date


class Mysql {

  constructor(config) {
    this.config = config
    this.connection = pool.getConnection(config).connection
  }

  async query(sql, values) {
    // console.log(await this.connection.format(sql, values))
    return await this.connection.promise().query(sql, values)
  }


  async insert(sql, values) {
    return this.query(sql, values)
  }

  async findOne(tableName, whereClause) {
    return this.query('SELECT * FROM ?? WHERE ? LIMIT 1', [tableName, whereClause])
      .then((result) => result[0])
      .then((result) => result[0])
  }

  async find(tableName, whereClause) {
    return this.query('SELECT * FROM ?? WHERE ?', [tableName, whereClause])
      .then(([result]) => result)
      .then((result) => result.map(row => row))
  }

  async isTableExists(tableName) {
    return this.connection.promise().query({
      sql: `SELECT table_name FROM information_schema.tables
        WHERE table_schema='${this.config.database}' AND table_name = '${tableName}'`
    })
      .then(([ result ]) => {
        return Array.isArray(result) && result.length > 0
      })
  }

  async isColumnExists(tableName, columnName) {
    return this.connection.promise().query({
      sql: `SELECT column_name FROM information_schema.columns
        WHERE table_schema='${this.config.database}' AND table_name = '${tableName}' AND column_name = '${columnName}'`
    })
      .then(([ result ]) => {
        return Array.isArray(result) && result.length > 0
      })
  }
}

module.exports = Mysql