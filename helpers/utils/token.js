const jwt = require('jsonwebtoken')
const getenv = require('getenv')
const { addDays, formatISO } = require('date-fns')

const secretKey = getenv('BASIC_AUTH_PASSWORD', 'supersecret')

const createToken = (payload, options = {}) => {
  const data = {
    accessToken: jwt.sign(payload, secretKey, options),
    expAccessToken: formatISO(addDays(new Date(), 1)),
  }
  return data
}

module.exports = {
  createToken
}