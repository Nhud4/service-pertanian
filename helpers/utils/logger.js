const {pino} = require('pino')
const { formatInTimeZone } = require('date-fns-tz')
const { ENV, timezone } = require('../../config')

const pinoOptions = {
  formatters: {
    bindings: () => ({})
  },
  timestamps: () => `, "time:":"${formatInTimeZone(new Date(), timezone, 'yyyy-MM-dd HH:mm:ss')}"`
}

if (ENV === 'development') {
  pinoOptions.transport = {
    target: 'pino-pretty'
  }
}

const pinoLogger = pino(pinoOptions)

const info = (context, message, scope, meta) => {
  pinoLogger.info({
    context,
    message,
    scope,
    meta
  })
}

const error = (context, message, scope, meta) => {
  pinoLogger.error({
    context,
    message,
    scope,
    meta
  })
}
const log = info

module.exports = {
  log,
  error,
  info
}