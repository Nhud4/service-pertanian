const angkaTerbilang = require('@develoka/angka-terbilang-js')
const { toInteger, isEmpty } = require('lodash')
const {formatInTimeZone} = require('date-fns-tz')

const convertLoketName = (loketName) => {
  const convert = parseInt(loketName) ? angkaTerbilang(toInteger(loketName)).split(' ').map((angka) => (`${angka}.WAV`)) : [`${loketName.toUpperCase()}.WAV`]
  return convert
}


const cleanElement = (obj) => {
  for (let propName in obj) {
    const values = typeof(obj[propName]) == 'number' ? obj[propName].toString() : obj[propName]
    if (isEmpty(values)) {
      delete obj[propName]
    }
  }
  return obj
}

const formatToUtc = (date) => {
  return formatInTimeZone(date, 'UTC', 'yyyy-MM-dd HH:mm:ss')
}

const setTotalTime = (voice) => {
  let totalTime = 0
  const abjad = []
  for (let i = 65; i <= 90; i++) {
    abjad.push(`${String.fromCharCode(i)}.WAV`)
  }

  voice.map((index) => {
    if (['call.WAV'].includes(index)) {
      totalTime += 4600
    } else if (['NOMORANTREAN.WAV'].includes(index)) {
      totalTime += 1600
    } else if (['keloket.WAV'].includes(index)) {
      totalTime += 1400
    } else if (abjad.includes(index)) {
      totalTime += 1100
    } else {
      totalTime += 1300
    }
  })
  return totalTime
}


module.exports = {
  cleanElement,
  convertLoketName,
  formatToUtc,
  setTotalTime
}