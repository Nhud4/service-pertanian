const CryptoJS = require('crypto-js')
const getenv = require('getenv')

const secretKey = getenv('BASIC_AUTH_PASSWORD', 'supersecret')

const hash = async(password) => {
  return CryptoJS.AES.encrypt(JSON.stringify(password), secretKey).toString()
}

const compare = async(password, hash) => {
  const bytes  = CryptoJS.AES.decrypt(hash, secretKey)
  const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
  return password === decryptedData
}

const decrypt = async(password) => {
  const bytes  = CryptoJS.AES.decrypt(password, secretKey)
  const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
  return decryptedData
}

module.exports = {
  hash,
  compare,
  decrypt
}