const response = (res, responseBody) => {
  if (!responseBody.code) {
    responseBody.code = 200
  }
  if (!responseBody.data) {
    responseBody.data = null
  }

  if (typeof responseBody.code === 'string') {
    responseBody.code = 500
  }

  res.status(responseBody.code).json({
    success: responseBody.code < 400,
    ...responseBody
  })
}

class NotFoundError {
  constructor(message) {
    this.message = message
    this.name = 'NotFoundError'
    this.code = 404
  }
}

class UnauthorizedError {
  constructor(message) {
    this.name = 'UnauthorizedError'
    this.code = 401
    this.message = message
  }
}

class UnprocessableEntityError {
  constructor(message, data) {
    this.message = message
    this.name = 'UnprocessableEntityError'
    this.code = 422
    this.data = data
  }
}

class InternalServerError {
  constructor(message) {
    this.message = message
    this.name = 'InternalServerError'
    this.code = 500
  }
}

module.exports = {
  response,
  UnauthorizedError,
  UnprocessableEntityError,
  NotFoundError,
  InternalServerError
}