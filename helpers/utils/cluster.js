const randomIterasi = (data, body) => {
  const iterasiData = {
    1: {
      c1 : data.filter((item) => item.kecId === body.iterasi1.c1)[0],
      c2 : data.filter((item) => item.kecId === body.iterasi1.c2)[0],
      c3 : data.filter((item) => item.kecId === body.iterasi1.c3)[0]
    },
    2: {
      c1 : data.filter((item) => item.kecId === body.iterasi2.c1)[0],
      c2 : data.filter((item) => item.kecId === body.iterasi2.c2)[0],
      c3 : data.filter((item) => item.kecId === body.iterasi2.c3)[0]
    }
  }

  return iterasiData
}

const countCluster = (data, iterasi, key, dataCl) => {
  let cluster = []
  let total = 0
  let c1 = []
  let c2 = []
  let c3 = []

  for (let i = 0; i < data.length; i++) {
    const count1 = Math.sqrt(
      ((data[i].tanam - iterasi[key].c1.tanam)**2) + 
        ((data[i].panen - iterasi[key].c1.panen)**2) +
        ((data[i].produksi - iterasi[key].c1.produksi)**2) +
        ((data[i].provitas - iterasi[key].c1.provitas)**2)
    )
    const count2 = Math.sqrt(
      ((data[i].tanam - iterasi[key].c2.tanam)**2) + 
        ((data[i].panen - iterasi[key].c2.panen)**2) +
        ((data[i].produksi - iterasi[key].c2.produksi)**2) +
        ((data[i].provitas - iterasi[key].c2.provitas)**2)
    )
    const count3 = Math.sqrt(
      ((data[i].tanam - iterasi[key].c3.tanam)**2) + 
        ((data[i].panen - iterasi[key].c3.panen)**2) +
        ((data[i].produksi - iterasi[key].c3.produksi)**2) +
        ((data[i].provitas - iterasi[key].c3.provitas)**2)
    )
      
    cluster.push({
      id: data[i].id,
      c1: count1,
      c2: count2,
      c3: count3,
      kedekatan: Math.min(count1, count2, count3)
    })

    total += Math.min(count1, count2, count3)

    c1.push(count1)
    c2.push(count2)
    c3.push(count3)
  }

  const newData = cluster.map((item) => ({
    ...item,
    cluster: c1.includes(item.kedekatan) 
      ? dataCl.filter((item) => item.code === 'C1').map((item) => item.id)[0] 
      : c2.includes(item.kedekatan) 
        ? dataCl.filter((item) => item.code === 'C2').map((item) => item.id)[0]
        : dataCl.filter((item) => item.code === 'C3').map((item) => item.id)[0]  
  }))

  return {total, data: newData}
}

module.exports = {
  countCluster,
  randomIterasi
}