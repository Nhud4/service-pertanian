const dotenv = require('dotenv/config')
const getenv = require('getenv')

const ENV = getenv('NODE_ENV', 'development')
const port = getenv.int('PORT', 3000)
const host = ENV === 'production' ? '0.0.0.0' : '127.0.0.1'
const timezone = getenv('TIMEZONE', 'UTC')

const basicAuth = {
  username: getenv('BASIC_AUTH_USERNAME', 'basicauth'),
  password: getenv('BASIC_AUTH_PASSWORD', 'supersecret')
}
const mysqlConfig = {
  host: getenv('MYSQL_HOST', 'localhost'),
  port: getenv.int('MYSQL_PORT', 3306),
  user: getenv('MYSQL_USER', 'root'),
  password: getenv('MYSQL_PASSWORD', 'supersecret'),
  database: getenv('MYSQL_DATABASE', 'dbname'),
  connectionLimit: getenv.int('MYSQL_CONNECTION_LIMIT', 10),
}

module.exports = {
  port,
  host,
  timezone,
  basicAuth,
  mysqlConfig,
}