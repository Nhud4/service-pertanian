const authMigration = require('../modules/auth/migration')
const kecMigration = require('../modules/region/migration')
const plantsMigration = require('../modules/plant/migration')
const clusterMigration = require('../modules/algorithm/migration')
const agricultureMigration = require('../modules/agriculture/migration')

const init = async() => {
  await authMigration()
  await kecMigration()
  await plantsMigration()
  await clusterMigration()
  await agricultureMigration()
}

module.exports = {
  init
}