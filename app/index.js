const express = require('express')
const cors = require('cors')
const {port, host, mysqlConfig} = require('../config/index')
const logger = require('../helpers/utils/logger')
const router = require('./router')
const { verifyBasicAuth } = require('../middlewares')
const {initMysqlPool} = require('../helpers/databases/mysql/connection')
const migration = require('./migration')
const { response } = require('../helpers/utils/response')

const app = express()

app.set('x-powered-by', false)
app.set('etag', false)
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())
app.use(router)
app.use(verifyBasicAuth)
app.use((err, req, res, next) => {
  if (err) {
    return response(res, err)
  }
  next()
})

const http = app.listen(port, host, () => {
  logger.log('express-codebase-backend-js', `⚡️[server]: Server is running at http://${host}:${port}`, 'Server Start')
})

initMysqlPool(mysqlConfig)

// migration
migration.init()