const { response } = require('../helpers/utils/response')
const Router = require('express-promise-router')
const {routeErrorHandler} = require('../middlewares/index')
const AuthRouter = require('../modules/auth/router')
const RegionRouter = require('../modules/region/router')
const ClusterRouter = require('../modules/algorithm/router')
const PlantRouter = require('../modules/plant/router')
const AgricultureRouter = require('../modules/agriculture/router')
const MigrationsRouter = require('../modules/migrations/router')
const DashboardRouter = require('../modules/dashboard/router')

const router = Router()

// import router module
router.use('/auth', AuthRouter)
router.use('/region', RegionRouter)
router.use('/cluster', ClusterRouter)
router.use('/plants', PlantRouter)
router.use('/agriculture', AgricultureRouter)
router.use('/migrations', MigrationsRouter)
router.use('/dashboard', DashboardRouter)

router.get('/', async(req, res) => {
  return response(res, {
    data: 'OK',
    code: 200, 
    message: 'Express codebase Runing propperly',
  })
})

router.use(routeErrorHandler)

module.exports = router