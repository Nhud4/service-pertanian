const Joi = require('joi')

const detail = Joi.object({
  params: Joi.object({
    id: Joi.number().required(),
  })
})

const count = Joi.object({
  body: Joi.object({
    tanamanId: Joi.number().integer().required(),
    iterasi1: Joi.object({
      c1: Joi.number().integer().required(),
      c2: Joi.number().integer().required(),
      c3: Joi.number().integer().required(),
    }).required(),
    iterasi2: Joi.object({
      c1: Joi.number().integer().required(),
      c2: Joi.number().integer().required(),
      c3: Joi.number().integer().required(),
    }).required()
  })
})

module.exports = {
  detail,
  count
}