const { response } = require('../../../helpers/utils/response')
const DB = require('../../../helpers/databases/mysql')
const Domain = require('./domain')
const { mysqlConfig } = require('../../../config/index')

const db = new DB(mysqlConfig)
const domain = new Domain(db)

const getList = async(req, res) => {
  const getData = await domain.getList(req.query)
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

const getDetail = async(req, res) => {
  const getData = await domain.getDetail(req.validated.params)
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

const clusterCount = async(req, res) => {
  const data = await domain.cluster(req.validated)
  return response(res, {
    data,
    message: 'Success'
  })
}

const iterasi = async(req, res) => {
  const getData = await domain.getIterasi()
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

module.exports = {
  getList,
  getDetail,
  clusterCount,
  iterasi
}