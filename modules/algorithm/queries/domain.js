const Query = require('./query')
const Command = require('../command/command')
const { isEmpty } = require('lodash')
const { NotFoundError, UnprocessableEntityError } = require('../../../helpers/utils/response')
const {countCluster, randomIterasi} = require('../../../helpers/utils/cluster')

class Domain {
  constructor(db) {
    this.query = new Query(db)
    this.command = new Command(db)
  }

  async getList() {
    const getData = await this.query.listData()
    if (isEmpty(getData)) return []

    const data = getData.map((item) => ({
      id: item.id,
      code: item.code,
      cluster: item.cluster,
      total: item.cp,
      active: item.cp > 0 ? true : false
    }))

    return data
  }

  async getDetail(payload) {
    const getData = await this.query.getById(payload.id)
    if (isEmpty(getData)) throw new NotFoundError('data not found')
    return getData
  }

  async getIterasi() {
    const getData = await this.query.getIterasi()
    return getData
  }

  async cluster(payload) {
    const {body} = payload

    const getData = await this.query.getDataPertanian(body.tanamanId)
    if (isEmpty(getData)) throw new NotFoundError('data not found')
    if (getData?.length < 28) throw new UnprocessableEntityError('data not match')

    const dataCl = await this.query.getCluster()

    const data = getData.map((item) => ({
      id: item.id,
      kecId: item.kecId,
      tanamanId: item.tanamanId,
      tanam: item.tanam,
      panen: item.panen,
      produksi: item.produksi,
      provitas: parseFloat(item.provitas)
    }))

    const iterasi = randomIterasi(data, body)
    const results = {
      1: countCluster(data, iterasi, 1, dataCl),
      2: countCluster(data, iterasi, 2, dataCl)
    }

    const difference = results[2].total - results[1].total
    if (difference > 0) {
      const date = new Date()
      const docInterasi = [
        [1, 'C1', iterasi[1].c1.id, date],
        [1, 'C2', iterasi[1].c2.id, date],
        [1, 'C3', iterasi[1].c3.id, date],
        [2, 'C1', iterasi[2].c1.id, date],
        [2, 'C2', iterasi[2].c2.id, date],
        [2, 'C3', iterasi[2].c3.id, date],
      ]

      const checkIterasi = await this.query.getIterasi()
      if (!isEmpty(checkIterasi)) {
        await this.command.deleteIterasi()
      }

      await this.command.insertIterasi(docInterasi)

      const result1 = results[1].data
      for (let i = 0; i < result1.length; i++) {
        await this.command.updateAgriculture(result1[i].cluster, result1[i].id)
      }
    }

    return {difference}
  }
}

module.exports = Domain