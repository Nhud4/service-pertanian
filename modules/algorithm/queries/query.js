class Query {
  constructor (db) {
    /**
     * @typedef {import('../../../helpers/databases/mysql')} DB
     * @type {DB}
     */

    this.db = db
  }

  async listData() {
    const sql = `SELECT cluster.id, code, cluster, COUNT(pertanian.id) AS cp FROM cluster
    LEFT JOIN pertanian ON pertanian.clusterId = cluster.id
    GROUP BY cluster.id`
    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async countData() {
    const sql = 'SELECT COUNT(id) AS count FROM cluster'
    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result[0]
  }

  async getById(id) {
    const sql = 'SELECT * FROM cluster WHERE id = ?'
    const result = await this.db.query(sql, [id])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result[0]
  }

  async getByCode(code) {
    const sql = 'SELECT * FROM cluster WHERE code = ?'
    const result = await this.db.query(sql, [code])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result[0]
  }

  async getIterasi(){
    const sql = `SELECT iterasi.*, kecamatan FROM iterasi
    INNER JOIN pertanian ON pertanian.id = iterasi.point
    INNER JOIN kecamatan ON pertanian.kecId = kecamatan.id`
    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async getDataPertanian(tanamanId) {
    const sql = 'SELECT * FROM pertanian WHERE tanamanId = ?'
    const result = await this.db.query(sql, [tanamanId])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async getCluster() {
    const sql = 'SELECT * FROM cluster'
    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }
}

module.exports = Query