const dummyData = [
  {
    id: 1,
    tahun: '2023',
    kecamatan: 'Sumberrejo',
    tanamanId: 2,
    tanam: 12240,
    panen: 12165,
    produksi: 80297.93,
    provitas: 6.6
  },
  {
    id: 2,
    tahun: '2023',
    kecamatan: 'Kepohbaru',
    tanamanId: 2,
    tanam: 12136,
    panen: 12135.5,
    produksi: 83410.28,
    provitas: 6.87
  },
  {
    id: 3,
    tahun: '2023',
    kecamatan: 'Kedungadem',
    tanamanId: 2,
    tanam: 11820,
    panen: 11820,
    produksi: 67003.56,
    provitas: 5.67
  },
  {
    id: 4,
    tahun: '2023',
    kecamatan: 'Dander',
    tanamanId: 2,
    tanam: 10027,
    panen: 9857,
    produksi: 61299.31,
    provitas: 6.22
  },
  {
    id: 5,
    tahun: '2023',
    kecamatan: 'Kalitidu',
    tanamanId: 2,
    tanam: 9565,
    panen: 7181,
    produksi: 46017.1,
    provitas: 6.41
  },
  {
    id: 6,
    tahun: '2023',
    kecamatan: 'Kanor',
    tanamanId: 2,
    tanam: 8791,
    panen: 6434,
    produksi: 42373.62,
    provitas: 6.59
  },
  {
    id: 7,
    tahun: '2023',
    kecamatan: 'Balen',
    tanamanId: 2,
    tanam: 8654,
    panen: 7354,
    produksi: 45396.38,
    provitas: 6.31
  },
  {
    id: 8,
    tahun: '2023',
    kecamatan: 'Baurid',
    tanamanId: 2,
    tanam: 8058,
    panen: 5065,
    produksi: 29108.95,
    provitas: 5.75
  },
  {
    id: 9,
    tahun: '2023',
    kecamatan: 'Sukosewu',
    tanamanId: 2,
    tanam: 6797,
    panen: 6624,
    produksi: 42393.6,
    provitas: 6.4
  },
  {
    id: 10,
    tahun: '2023',
    kecamatan: 'Ngasem',
    tanamanId: 2,
    tanam: 6402,
    panen: 6213,
    produksi: 40312.17,
    provitas: 6.49
  },
  {
    id: 11,
    tahun: '2023',
    kecamatan: 'Kapas',
    tanamanId: 2,
    tanam: 6061,
    panen: 5688,
    produksi: 38678.4,
    provitas: 6.8
  },
  {
    id: 12,
    tahun: '2023',
    kecamatan: 'Ngraho',
    tanamanId: 2,
    tanam: 5920,
    panen: 5468,
    produksi: 27378.17,
    provitas: 5.01
  },
  {
    id: 13,
    tahun: '2023',
    kecamatan: 'Tambakrejo',
    tanamanId: 2,
    tanam: 5723,
    panen: 5723,
    produksi: 33236.7,
    provitas: 5.81
  },
  {
    id: 14,
    tahun: '2023',
    kecamatan: 'Sugihwaras',
    tanamanId: 2,
    tanam: 5008,
    panen: 5008,
    produksi: 26836.56,
    provitas: 5.36
  },
  {
    id: 15,
    tahun: '2023',
    kecamatan: 'Gayam',
    tanamanId: 2,
    tanam: 4665,
    panen: 3662,
    produksi: 22131.55,
    provitas: 6.04
  },
  {
    id: 16,
    tahun: '2023',
    kecamatan: 'Malo',
    tanamanId: 2,
    tanam: 4582,
    panen: 4270,
    produksi: 24578.71,
    provitas: 5.76
  },
  {
    id: 17,
    tahun: '2023',
    kecamatan: 'Temayang',
    tanamanId: 2,
    tanam: 4228,
    panen: 4228,
    produksi: 26372,
    provitas: 6.24
  },
  {
    id: 18,
    tahun: '2023',
    kecamatan: 'Kasiman',
    tanamanId: 2,
    tanam: 3968,
    panen: 2912,
    produksi: 16318.46,
    provitas: 5.6
  },
  {
    id: 19,
    tahun: '2023',
    kecamatan: 'Padangan',
    tanamanId: 2,
    tanam: 3880,
    panen: 3553,
    produksi: 21318,
    provitas: 6
  },
  {
    id: 20,
    tahun: '2023',
    kecamatan: 'Purwosari',
    tanamanId: 2,
    tanam: 3280,
    panen: 3270,
    produksi: 21158.1,
    provitas: 6.47
  },
  {
    id: 21,
    tahun: '2023',
    kecamatan: 'Trucuk',
    tanamanId: 2,
    tanam: 2342,
    panen: 1579,
    produksi: 9289.24,
    provitas: 5.88
  },
  {
    id: 22,
    tahun: '2023',
    kecamatan: 'Margomulyo',
    tanamanId: 2,
    tanam: 2164,
    panen: 2164.46,
    produksi: 11061.99,
    provitas: 5.11
  },
  {
    id: 23,
    tahun: '2023',
    kecamatan: 'Gondang',
    tanamanId: 2,
    tanam: 1936,
    panen: 1916,
    produksi: 11063.12,
    provitas: 5.77
  },
  {
    id: 24,
    tahun: '2023',
    kecamatan: 'Bojonegoro',
    tanamanId: 2,
    tanam: 1719,
    panen: 1275,
    produksi: 7812.84,
    provitas: 6.13
  },
  {
    id: 25,
    tahun: '2023',
    kecamatan: 'Bubulan',
    tanamanId: 2,
    tanam: 916,
    panen: 916,
    produksi: 5638.7,
    provitas: 6.16
  },
  {
    id: 26,
    tahun: '2023',
    kecamatan: 'Kedewan',
    tanamanId: 2,
    tanam: 834,
    panen: 834,
    produksi: 4098.63,
    provitas: 4.91
  },
  {
    id: 27,
    tahun: '2023',
    kecamatan: 'Ngambon',
    tanamanId: 2,
    tanam: 717,
    panen: 577,
    produksi: 4255.97,
    provitas: 5.94
  },
  {
    id: 28,
    tahun: '2023',
    kecamatan: 'Sekar',
    tanamanId: 2,
    tanam: 696,
    panen: 686,
    produksi: 3341.48,
    provitas: 4.87
  }
]

module.exports = dummyData