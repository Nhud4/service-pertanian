const Router = require('express-promise-router')
const {verifyBearerAuth} = require('../../middlewares/bearerAuth')
const {verifyBasicAuth} = require('../../middlewares/basicAuth')
const queriesHandler = require('./queries/handler')
const commandHandler = require('./command/handler')
const {insertData, updateData} = require('./command/schema')
const {detail, count} = require('./queries/schema')
const { validateSchema } = require('../../middlewares/index')

const router = Router()

router.get('/list', verifyBearerAuth, queriesHandler.getList)
router.get('/iterasi', verifyBearerAuth, queriesHandler.iterasi)
router.get('/detail/:id', verifyBearerAuth, validateSchema(detail), queriesHandler.getDetail)
router.post('/count', verifyBasicAuth, validateSchema(count), queriesHandler.clusterCount)
router.post('/add', verifyBearerAuth, validateSchema(insertData), commandHandler.insertData)
router.put('/edit/:id', verifyBearerAuth, validateSchema(updateData), commandHandler.updateData)
router.delete('/delete/:id', verifyBearerAuth, validateSchema(detail), commandHandler.deleteData)

module.exports = router