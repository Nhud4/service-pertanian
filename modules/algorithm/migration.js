const DB = require('../../helpers/databases/mysql')
const {mysqlConfig} = require('../../config/index')

const db = new DB(mysqlConfig)

const cluster = async() => {
  const statement = {
    sql: `CREATE TABLE IF NOT EXISTS \`cluster\` (
        \`id\` INT UNSIGNED NOT NULL AUTO_INCREMENT,
        \`code\` VARCHAR(255),
        \`cluster\` VARCHAR(255),
        PRIMARY KEY (\`id\`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`,
    values: []
  }
  await db.query(statement)
}

const init = async() => {
  await cluster()
}

module.exports = init