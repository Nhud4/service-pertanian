class Command {
  constructor(db) {
    /**
         * @typedef {import('../../../helpers/databases/mysql')} DB
         * @type {DB}
         */

    this.db = db
  }

  async insertData(document) {
    const sql = 'INSERT INTO cluster (code, cluster) VALUES (?, ?)'
    const values = [
      document.code,
      document.cluster
    ]
    const result = await this.db.query(sql,  values)
    return result
  }

  async updateData(document, params) {
    const sql = 'UPDATE cluster SET ? WHERE id = ?'
    const values = [document, params.id]
    const result = await this.db.query({sql, values})
    return result[0]
  }

  async updateAgriculture(clusterId, id) {
    const sql = 'UPDATE pertanian SET clusterId = ? WHERE id = ?'
    const values = [clusterId, id]
    const result = await this.db.query({sql, values})
    return result[0]
  }

  async insertIterasi(document) {
    const sql = 'INSERT INTO iterasi (number, code, point, date) VALUES ?'
    const values = [document]
    const result = await this.db.query({sql, values})
    return result[0]
  }

  async deleteIterasi() {
    const sql = 'DELETE FROM iterasi WHERE number = 1 OR number = 2'
    const values = []
    const result = await this.db.query({sql, values})
    return result[0]
  }

  async deleteCLuster(params) {
    const sql = 'DELETE FROM cluster WHERE id = ?'
    const values = [params.id]
    const result = await this.db.query({sql, values})
    return result[0]
  }
}

module.exports = Command