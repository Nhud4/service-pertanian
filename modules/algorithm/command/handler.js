const { response } = require('../../../helpers/utils/response')
const DB = require('../../../helpers/databases/mysql')
const Domain = require('./domain')
const { mysqlConfig } = require('../../../config/index')

const db = new DB(mysqlConfig)
const domain = new Domain(db)

const insertData = async(req, res) => {
  const insertData = await domain.insert(req.validated)
  return response(res, {
    data: insertData,
    message: 'Success'
  })
}

const updateData = async(req, res) => {
  const insertData = await domain.update(req.validated)
  return response(res, {
    data: insertData,
    message: 'Success'
  })
}

const deleteData = async(req, res) => {
  const insertData = await domain.delete(req.validated)
  return response(res, {
    data: insertData,
    message: 'Success'
  })
}

module.exports = {
  insertData,
  updateData,
  deleteData
}