const Joi = require('joi')

const insertData = Joi.object({
  body: Joi.object({
    cluster: Joi.string().required()
  })
})

const updateData = Joi.object({
  params: Joi.object({
    id: Joi.number().integer().required()
  }),
  body: Joi.object({
    cluster: Joi.string().required()
  })
})


module.exports = {
  insertData,
  updateData
}