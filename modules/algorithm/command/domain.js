const Command = require('./command')
const Query = require('../queries/query')
const { isEmpty } = require('lodash')
const { NotFoundError } = require('../../../helpers/utils/response')

class Domain {
  constructor(db) {
    this.command = new Command(db)
    this.query = new Query(db)
  }

  async insert(payload) {
    const {body} = payload
    
    const total = await this.query.countData()

    const document = {
      code: `C${total.count + 1}`,
      cluster: body.cluster.toLowerCase()
    }

    const insertData = await this.command.insertData(document)
    return insertData
  }

  async update(payload) {
    const {body, params} = payload
    
    const checkData = await this.query.getById(params.id)
    if (isEmpty(checkData)) throw new NotFoundError('data not found')

    const document = {
      cluster: body.cluster.toLowerCase()
    }
    
    const updateData = await this.command.updateData(document, params)
    return updateData
  }

  async delete(payload) {
    const {params} = payload
    
    const checkData = await this.query.getById(params.id)
    if (isEmpty(checkData)) throw new NotFoundError('data not found')
    
    const insertData = await this.command.deleteCLuster(params)
    return insertData
  }
}

module.exports = Domain