const Joi = require('joi')

const insertData = Joi.object({
  body: Joi.object({
    region: Joi.string().required(),
  })
})

const updateData = Joi.object({
  params: Joi.object({
    id: Joi.number().integer().required()
  }),
  body: Joi.object({
    region: Joi.string().required(),
  })
})

const deleteData = Joi.object({
  params: Joi.object({
    id: Joi.number().integer().required()
  }),
})

module.exports = {
  insertData,
  updateData,
  deleteData
}