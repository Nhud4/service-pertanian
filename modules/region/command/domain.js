const Command = require('./command')
const Query = require('../queries/query')
const { isEmpty } = require('lodash')
const { UnprocessableEntityError, NotFoundError } = require('../../../helpers/utils/response')

class Domain {
  constructor(db) {
    this.command = new Command(db)
    this.query = new Query(db)
  }

  async insert(payload) {
    const {body} = payload
    
    const checkData = await this.query.getByKecamatan(body.region)
    if (!isEmpty(checkData)) throw new UnprocessableEntityError('data already exists')

    const document = {
      kecamatan: body.region.toLowerCase()
    }
    const insertData = await this.command.insertData(document)
    return insertData
  }

  async update(payload) {
    const {body, params} = payload

    const checkId = await this.query.getById(params.id)
    if (isEmpty(checkId)) throw new NotFoundError('data no found')

    const document = {
      kecamatan: body.region.toLowerCase()
    }
    
    const updateData = await this.command.updateData(document, params)
    return updateData
  }

  async delete(payload) {
    const {params} = payload

    const checkId = await this.query.getById(params.id)
    if (isEmpty(checkId)) throw new NotFoundError('data no found')

    const updateData = await this.command.deleteData(params)
    return updateData
  }
}

module.exports = Domain