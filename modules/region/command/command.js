class Command {
  constructor(db) {
    /**
         * @typedef {import('../../../helpers/databases/mysql')} DB
         * @type {DB}
         */

    this.db = db
  }

  async insertData(document) {
    const sql = 'INSERT INTO kecamatan (kecamatan) VALUES (?)'
    const result = await this.db.query(sql,  [document.kecamatan])
    return result[0]
  }

  async updateData(document, params) {
    const sql = 'UPDATE kecamatan SET kecamatan = ? WHERE id = ?'
    const values = [document.kecamatan, params.id]
    const result = await this.db.query(sql,  values)
    return result[0]
  }

  async deleteData(params) {
    const sql = 'DELETE FROM kecamatan WHERE id = ?'
    const values = [params.id]
    const result = await this.db.query({sql, values})
    return result[0]
  }
}

module.exports = Command