const { response } = require('../../../helpers/utils/response')
const DB = require('../../../helpers/databases/mysql')
const Domain = require('./domain')
const { mysqlConfig } = require('../../../config/index')

const db = new DB(mysqlConfig)
const domain = new Domain(db)

const getList = async(req, res) => {
  const getData = await domain.getList(req.validated)
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

module.exports = {
  getList,
}