class Query {
  constructor (db) {
    /**
     * @typedef {import('../../../helpers/databases/mysql')} DB
     * @type {DB}
     */

    this.db = db
  }

  async listData(query) {
    let sql = `SELECT kecamatan.id, kecamatan, COUNT(pertanian.id) as cp FROM kecamatan
    LEFT JOIN pertanian ON pertanian.kecId = kecamatan.id`

    if (query.search) {
      sql += ` WHERE kecamatan LIKE '%${query.search}%'`
    }
    
    sql += ' GROUP BY kecamatan.id'

    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async getByKecamatan(kecamatan) {
    const sql = 'SELECT * FROM kecamatan WHERE kecamatan = ?'
    const result = await this.db.query(sql, [kecamatan])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async getById(id) {
    const sql = 'SELECT * FROM kecamatan WHERE id = ?'
    const result = await this.db.query(sql, [id])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }
}

module.exports = Query