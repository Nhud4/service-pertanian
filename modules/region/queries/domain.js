const Query = require('./query')
const { isEmpty } = require('lodash')

class Domain {
  constructor(db) {
    this.query = new Query(db)
  }

  async getList(payload) {
    const {query} = payload

    const getData = await this.query.listData(query)
    if (isEmpty(getData)) return []

    const data = getData.map((item) => ({
      id: item.id,
      code: `KC-${item.id >= 10 ? item.id: `0${item.id}`}`,
      region: item.kecamatan,
      active: item.cp > 0 ? true : false
    }))
    return data
  }
}

module.exports = Domain