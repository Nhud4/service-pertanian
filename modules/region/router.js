const Router = require('express-promise-router')
const {verifyBearerAuth} = require('../../middlewares/bearerAuth')
const queriesHandler = require('./queries/handler')
const commandHandler = require('./command/handler')
const {insertData, updateData, deleteData} = require('./command/schema')
const {listData} = require('./queries/schema')
const { validateSchema } = require('../../middlewares/index')


const router = Router()

router.get('/list', verifyBearerAuth, validateSchema(listData), queriesHandler.getList)
router.post('/add', verifyBearerAuth, validateSchema(insertData), commandHandler.insertData)
router.put('/update/:id', verifyBearerAuth, validateSchema(updateData), commandHandler.updateData)
router.delete('/delete/:id', verifyBearerAuth, validateSchema(deleteData), commandHandler.deleteData)

module.exports = router