const DB = require('../../helpers/databases/mysql')
const {mysqlConfig} = require('../../config/index')

const db = new DB(mysqlConfig)

const kecamatan = async() => {
  const statement = {
    sql: `CREATE TABLE IF NOT EXISTS \`kecamatan\` (
        \`id\` INT UNSIGNED NOT NULL AUTO_INCREMENT,
        \`kecamatan\` VARCHAR(255),
        PRIMARY KEY (\`id\`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`,
    values: []
  }
  await db.query(statement)
}

const init = async() => {
  await kecamatan()
}

module.exports = init