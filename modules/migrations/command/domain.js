const Command = require('./command')
const dummyData = require('../../algorithm/data')

class Domain {
  constructor(db) {
    this.command = new Command(db)
  }

  async insertMasterUser() {
    const document = dummyData.map((item) => {
      return item.produksi
    })

    // const insertData = await this.command.insertUser(document)
    return document
  }

  async insertPertanian(payload) {
    const document = dummyData.map((item, index) => {
      return [
        item.tahun,
        index + 2,
        item.tanamanId,
        item.tanam,
        item.panen,
        item.produksi,
        `${item.provitas}`,
        payload.id
      ]
    })

    const insertData = await this.command.insertPertanian(document)
    return insertData
  }
}

module.exports = Domain