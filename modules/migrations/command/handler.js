const { response } = require('../../../helpers/utils/response')
const DB = require('../../../helpers/databases/mysql')
const Domain = require('./domain')
const { mysqlConfig } = require('../../../config/index')

const db = new DB(mysqlConfig)
const domain = new Domain(db)

const insertMasterData = async(req, res) => {
  const insertData = await domain.insertMasterUser()
  return response(res, {
    data: insertData,
    message: 'Success'
  })
}

const insertPertanian = async(req, res) => {
  const insertData = await domain.insertPertanian(req.user)
  return response(res, {
    data: insertData,
    message: 'Success'
  })
}

module.exports = {
  insertMasterData,
  insertPertanian
}