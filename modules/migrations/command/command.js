class Command {
  constructor(db) {
    /**
         * @typedef {import('../../../helpers/databases/mysql')} DB
         * @type {DB}
         */

    this.db = db
  }

  async insertUser(document) {
    const sql = 'INSERT INTO kecamatan (kecamatan) VALUES ?'
    const values = [document]
    const result = await this.db.query(sql, values)
    return result
  }

  async insertPertanian(document) {
    const sql = `INSERT INTO pertanian (tahun, kecId, tanamanId, tanam, panen, produksi, provitas, createdBy) 
    VALUES ?`
    const values = [document]
    const result = await this.db.query(sql,  values)
    return result
  }
}

module.exports = Command