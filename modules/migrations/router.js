const Router = require('express-promise-router')
const commandHandler = require('./command/handler')
const {verifyBearerAuth} = require('../../middlewares/bearerAuth')

const router = Router()

router.post('/region', verifyBearerAuth, commandHandler.insertMasterData)
router.post('/pertanian', verifyBearerAuth, commandHandler.insertPertanian)

module.exports = router