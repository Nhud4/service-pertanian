const DB = require('../../helpers/databases/mysql')
const {mysqlConfig} = require('../../config/index')

const db = new DB(mysqlConfig)

const userCategory = async() => {
  const statement = {
    sql: `CREATE TABLE IF NOT EXISTS \`userCategory\` (
        \`id\` INT UNSIGNED NOT NULL AUTO_INCREMENT,
        \`category\` VARCHAR(255),
        PRIMARY KEY (\`id\`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`,
    values: []
  }
  await db.query(statement)
}

const user = async() => {
  const statement = {
    sql: `CREATE TABLE IF NOT EXISTS \`user\` (
        \`id\` INT UNSIGNED NOT NULL AUTO_INCREMENT,
        \`nama\` VARCHAR(255),
        \`username\` VARCHAR(255),
        \`password\` VARCHAR(255),
        \`catId\` INT UNSIGNED,
        PRIMARY KEY (\`id\`),
        FOREIGN KEY (\`catId\`) REFERENCES \`userCategory\` (\`id\`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`,
    values: []
  }
  await db.query(statement)
}

const init = async() => {
  await userCategory()
  await user()
}

module.exports = init