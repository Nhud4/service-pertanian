const {hash, compare} = require('../../../helpers/utils/password')
const Command = require('./command')
const Query = require('../queries/query')
const { isEmpty } = require('lodash')
const { NotFoundError, UnprocessableEntityError } = require('../../../helpers/utils/response')
const {createToken} = require('../../../helpers/utils/token')

class Domain {
  constructor(db) {
    this.command = new Command(db)
    this.query = new Query(db)
  }

  async insertMasterUser(payload) {
    const {body} = payload
    const hashPassword = await hash(body.password)
    const document = {
      name: body.name,
      username: body.username,
      password: hashPassword,
      catId: body.userCategory
    }
    const insertData = await this.command.insertUser(document)
    return insertData
  }

  async updateMasterUser(payload) {
    const {body, params} = payload
    const checkData = await this.query.getById(params.id)
    if (isEmpty(checkData)) throw new NotFoundError('data not found')

    const comparePassword = await compare(body.oldPassword, checkData.password)
    if (!comparePassword) throw new UnprocessableEntityError('old password is wrong')

    const hashPassword = await hash(body.newPassword)
    const document = {
      username: body.username, 
      nama: body.name, 
      password: hashPassword
    }
    const updateData = await this.command.updatePassword(document, params)
    return updateData
  }

  async login(payload) {
    const {body} = payload
    const checkData = await this.query.checkUsername(body.username)
    if (isEmpty(checkData)) throw new NotFoundError('username not found')
        
    const comparePassword = await compare(body.password, checkData.password)
    if (!comparePassword) throw new UnprocessableEntityError('username or password is wrong')
    const payloadToken = {
      id: checkData.id,
      username: checkData.username,
    }
    const data = createToken(payloadToken, { expiresIn: '1d' })
    return data
  }

  async delete(payload) {
    const {params} = payload

    const checkData = await this.query.getById(params.id)
    if (isEmpty(checkData)) throw new NotFoundError('data not found')

    const deleteData = await this.command.deleteData(params)
    return deleteData
  }
}

module.exports = Domain