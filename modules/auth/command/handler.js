const { response } = require('../../../helpers/utils/response')
const DB = require('../../../helpers/databases/mysql')
const Domain = require('./domain')
const { mysqlConfig } = require('../../../config/index')

const db = new DB(mysqlConfig)
const domain = new Domain(db)

const insertMasterUser = async(req, res) => {
  const insertData = await domain.insertMasterUser(req.validated)
  return response(res, {
    data: insertData,
    message: 'Success'
  })
}

const updatePassword = async(req, res) => {
  const updateData = await domain.updateMasterUser(req.validated)
  return response(res, {
    data: updateData,
    message: 'Success'
  })
}

const loginUser = async(req, res) => {
  const data = await domain.login(req.validated)
  return response(res, {
    data,
    message: 'Success'
  })
}

const deleteData = async(req, res) => {
  const data = await domain.delete(req.validated)
  return response(res, {
    data,
    message: 'Success'
  })
}

module.exports = {
  insertMasterUser,
  updatePassword,
  loginUser,
  deleteData
}