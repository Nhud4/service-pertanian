class Command {
  constructor(db) {
    /**
         * @typedef {import('../../../helpers/databases/mysql')} DB
         * @type {DB}
         */

    this.db = db
  }

  async insertUser(document) {
    const sql = 'INSERT INTO user (nama, username, password, catId) VALUES (?, ? , ?, ?)'
    const values = [
      document.name, 
      document.username, 
      document.password, 
      document.catId
    ]
    const result = await this.db.query(sql, values)
    return result
  }

  async updatePassword(document, params) {
    const sql = 'UPDATE user SET ? WHERE id = ?'
    const values = [document, params.id]
    try {
      const result = await this.db.query(sql, values)
      return result
    } catch (err) {
      console.log(err)
    }
  }

  async deleteData(params) {
    const sql = 'DELETE FROM user WHERE id = ?'
    const values = [params.id]
    const result = await this.db.query({sql, values})
    return result[0]
  }
}

module.exports = Command