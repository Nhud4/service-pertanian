const Joi = require('joi')

const insertData = Joi.object({
  body: Joi.object({
    name: Joi.string().min(3).required(),
    username: Joi.string().min(6).required(),
    password: Joi.string().min(6).required(),
    userCategory: Joi.number().integer().required()
  })
})

const login = Joi.object({
  body: Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required()
  })
})

const updateData = Joi.object({
  params: Joi.object({
    id: Joi.number().integer().required()
  }),
  body: Joi.object({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().min(6).required(),
    name: Joi.string().required(),
    username: Joi.string().required()
  })
})

module.exports = {
  insertData,
  login,
  updateData
}