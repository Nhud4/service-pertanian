class Query {
  constructor (db) {
    /**
     * @typedef {import('../../../helpers/databases/mysql')} DB
     * @type {DB}
     */

    this.db = db
  }

  async listData() {
    const sql = 'SELECT * FROM user'
    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async checkUsername(username) {
    const sql = 'SELECT * FROM user WHERE username = ?'
    const result = await this.db.query(sql, [username])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result[0]
  }

  async getById(id) {
    const sql = `SELECT user.*, category FROM user 
    INNER JOIN userCategory ON user.catId = userCategory.id
    WHERE user.id = ?`
    const result = await this.db.query(sql, [id])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result[0]
  }

  async getProfile(id) {
    const sql = `SELECT user.id, nama, username, category from user
    INNER JOIN userCategory ON user.catId = userCategory.id
    WHERE user.id = ?`
    const result = await this.db.query(sql, [id])
      .then(([result]) => result)
      .then((result) => result.map(row => row))
    return result[0]
  }
}

module.exports = Query