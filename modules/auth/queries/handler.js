const { response } = require('../../../helpers/utils/response')
const DB = require('../../../helpers/databases/mysql')
const Domain = require('./domain')
const { mysqlConfig } = require('../../../config/index')

const db = new DB(mysqlConfig)
const domain = new Domain(db)

const getDetail = async(req, res) => {
  const getData = await domain.getById(req.validated)
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

const getList = async(req, res) => {
  const getData = await domain.getList(req.query)
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

const profile = async(req, res) => {
  const payload = {...req.user}
  const getData = await domain.getProfile(payload)
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

module.exports = {
  getDetail,
  getList,
  profile
}