const { isEmpty } = require('lodash')
const Query = require('./query')
const { NotFoundError } = require('../../../helpers/utils/response')
const {decrypt} = require('../../../helpers/utils/password')

class Domain {
  constructor(db) {
    this.query = new Query(db)
  }

  async getById(payload) {
    const {params} = payload

    const getData = await this.query.getById(params.id)
    if (isEmpty(getData)) throw new NotFoundError('data not found')

    const password = await decrypt(getData.password)
    
    const data = {
      id: getData.id,
      name: getData.nama,
      username: getData.username,
      userCategory: getData.category,
      password,
    }
    return data
  }

  async getList() {
    const getData = await this.query.listData()
    if (isEmpty(getData)) return []
    const data = getData.map((item) => ({
      id: item.id,
      nama: item.nama,
      username: item.username
    }))
    return data
  }

  async getProfile(payload) {
    const getData = await this.query.getProfile(payload.id)
    if (isEmpty(getData)) throw new NotFoundError('data not found')
    const data = {
      id: getData.id,
      nama: getData.nama,
      username: getData.username,
      userCategory: getData.category
    }
    return data
  }
}

module.exports = Domain