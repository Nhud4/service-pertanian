const Joi = require('joi')

const detail = Joi.object({
  params: Joi.object({
    id: Joi.number().integer().required()
  })
})

module.exports = {
  detail
}