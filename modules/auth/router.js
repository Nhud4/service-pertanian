const Router = require('express-promise-router')
const commandHandler = require('./command/handler')
const queriesHandler = require('./queries/handler')
const { validateSchema } = require('../../middlewares/index')
const {insertData, login, updateData} = require('./command/schema')
const {detail} = require('./queries/schema')
const {verifyBearerAuth} = require('../../middlewares/bearerAuth')
const {verifyBasicAuth} = require('../../middlewares/basicAuth')

const router = Router()

router.get('/list', verifyBearerAuth, queriesHandler.getList)
router.get('/detail/:id',verifyBearerAuth, validateSchema(detail), queriesHandler.getDetail)
router.post('/login', verifyBasicAuth, validateSchema(login), commandHandler.loginUser)
router.post('/register', verifyBasicAuth, validateSchema(insertData), commandHandler.insertMasterUser)
router.put('/update/:id',verifyBearerAuth, validateSchema(updateData), commandHandler.updatePassword)
router.get('/profile', verifyBearerAuth, queriesHandler.profile)
router.delete('/delete/:id', verifyBasicAuth, validateSchema(detail), commandHandler.deleteData)

module.exports = router