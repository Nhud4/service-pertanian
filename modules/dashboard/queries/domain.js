const Query = require('./query')

class Domain {
  constructor(db) {
    this.query = new Query(db)
  }

  async summary() {
    const pertanian = await this.query.countPertanian()
    const kecamatan = await this.query.countRegion()
    const tanaman = await this.query.countPlants()
    const laporan = await this.query.countReport()

    const data = {
      pertanian: pertanian[0].count,
      kecamatan: kecamatan[0].count,
      tanaman: tanaman[0].count,
      laporan: laporan[0].count
    }

    return data
  }

  async tanaman() {
    const getData = await this.query.tanaman()
    const data = getData.map((item) => ({
      tanaman: item.jenis,
      tTanam: parseInt(item.tTanam)|| 0,
      tPanen: parseInt(item.tPanen) || 0,
      tProduksi: parseInt(item.tProduksi) || 0
    }))
    return data
  }

  async cluster() {
    const getData = await this.query.cluster()
    return getData
  }

  async kecamatan() {
    const getData = await this.query.kecamatan()
    const data = getData.map((item) => ({
      kecamatan: item.kecamatan,
      tTanam: parseInt(item.tTanam)|| 0,
      tPanen: parseInt(item.tPanen) || 0,
      tProduksi: parseInt(item.tProduksi) || 0
    }))
    return data
  }
}

module.exports = Domain