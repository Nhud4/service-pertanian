const Joi = require('joi')

const listData = Joi.object({
  query: Joi.object({
    search: Joi.string().allow('', null).default(null),
  })
})


module.exports = {
  listData
}