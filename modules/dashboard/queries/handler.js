const { response } = require('../../../helpers/utils/response')
const DB = require('../../../helpers/databases/mysql')
const Domain = require('./domain')
const { mysqlConfig } = require('../../../config/index')

const db = new DB(mysqlConfig)
const domain = new Domain(db)

const summary = async(req, res) => {
  const getData = await domain.summary()
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

const tanaman = async(req, res) => {
  const getData = await domain.tanaman()
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

const cluster = async(req, res) => {
  const getData = await domain.cluster()
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

const kecamatan = async(req, res) => {
  const getData = await domain.kecamatan()
  return response(res, {
    data: getData,
    message: 'Success'
  })
}

module.exports = {
  summary,
  tanaman,
  cluster,
  kecamatan
}