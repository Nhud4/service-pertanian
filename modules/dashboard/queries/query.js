class Query {
  constructor (db) {
    /**
     * @typedef {import('../../../helpers/databases/mysql')} DB
     * @type {DB}
     */

    this.db = db
  }

  async countPertanian() {
    const sql = 'SELECT COUNT(*) AS count from pertanian'

    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async countRegion() {
    const sql = 'SELECT COUNT(*) AS count from kecamatan'

    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async countPlants() {
    const sql = 'SELECT COUNT(*) AS count from tanaman'

    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async countReport() {
    const sql = 'SELECT COUNT(*) AS count from pertanian WHERE clusterId IS NOT NULL'

    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async tanaman() {
    const sql = `SELECT SUM(tanam) AS tTanam, SUM(panen) AS tPanen, SUM(produksi) AS tProduksi, jenis 
    FROM pertanian
    RIGHT JOIN tanaman ON tanaman.id = pertanian.tanamanId
    GROUP BY tanaman.id`

    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async cluster() {
    const sql = `SELECT COUNT(pertanian.id) AS count, code, cluster FROM pertanian
    RIGHT JOIN cluster ON cluster.id = pertanian.clusterId
    GROUP BY cluster.id`
    
    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async kecamatan() {
    const sql = `SELECT SUM(tanam) AS tTanam, SUM(panen) AS tPanen, SUM(produksi) AS tProduksi, kecamatan FROM pertanian
    RIGHT JOIN kecamatan ON kecamatan.id = pertanian.kecId
    GROUP BY kecamatan.id`
    
    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }
}

module.exports = Query