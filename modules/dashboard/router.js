const Router = require('express-promise-router')
const {verifyBearerAuth} = require('../../middlewares/bearerAuth')
const queriesHandler = require('./queries/handler')

const router = Router()

router.get('/summary', verifyBearerAuth, queriesHandler.summary)
router.get('/tanaman', verifyBearerAuth, queriesHandler.tanaman)
router.get('/cluster', verifyBearerAuth, queriesHandler.cluster)
router.get('/kecamatan', verifyBearerAuth, queriesHandler.kecamatan)

module.exports = router