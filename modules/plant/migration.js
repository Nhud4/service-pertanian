const DB = require('../../helpers/databases/mysql')
const {mysqlConfig} = require('../../config/index')

const db = new DB(mysqlConfig)

const plants = async() => {
  const statement = {
    sql: `CREATE TABLE IF NOT EXISTS \`tanaman\` (
        \`id\` INT UNSIGNED NOT NULL AUTO_INCREMENT,
        \`jenis\` VARCHAR(255),
        PRIMARY KEY (\`id\`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`,
    values: []
  }
  await db.query(statement)
}

const init = async() => {
  await plants()
}

module.exports = init