class Query {
  constructor (db) {
    /**
     * @typedef {import('../../../helpers/databases/mysql')} DB
     * @type {DB}
     */

    this.db = db
  }

  async listData(query) {
    let sql = `SELECT tanaman.id, jenis, COUNT(pertanian.id) as cp FROM tanaman
    LEFT JOIN pertanian ON pertanian.tanamanId = tanaman.id`

    if (query.search) {
      sql += ` WHERE jenis LIKE '%${query.search}%'`
    }
    
    sql += ' GROUP BY tanaman.id'

    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async getById(id) {
    const sql = 'SELECT * FROM tanaman WHERE id = ?'
    const result = await this.db.query(sql, [id])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result[0]
  }

  async getByType(type) {
    const sql = 'SELECT * FROM tanaman WHERE jenis = ?'
    const result = await this.db.query(sql, [type])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result[0]
  }
}

module.exports = Query