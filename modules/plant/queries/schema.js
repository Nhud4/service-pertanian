const Joi = require('joi')

const listData = Joi.object({
  query: Joi.object({
    search: Joi.string().allow('', null).default(null),
  })
})

const detail = Joi.object({
  params: Joi.object({
    id: Joi.number().required(),
  })
})

module.exports = {
  detail,
  listData
}