const Query = require('./query')
const { isEmpty } = require('lodash')
const { NotFoundError } = require('../../../helpers/utils/response')

class Domain {
  constructor(db) {
    this.query = new Query(db)
  }

  async getList(payload) {
    const {query} = payload

    const getData = await this.query.listData(query)
    if (isEmpty(getData)) return []

    const data = getData.map((item) => ({
      id: item.id,
      code: `TC-${item.id >= 10 ? `0${item.id}`: `00${item.id}`}`,
      jenis: item.jenis,
      active: item.cp > 0 ? true : false
    }))

    return data
  }

  async getDetail(payload) {
    const getData = await this.query.getById(payload.id)
    if (isEmpty(getData)) throw new NotFoundError('data not found')
    return getData
  }
}

module.exports = Domain