const Joi = require('joi')

const insertData = Joi.object({
  body: Joi.object({
    jenis: Joi.string().required(),
  })
})

const updateData = Joi.object({
  params: Joi.object({
    id: Joi.number().integer().required()
  }),
  body: Joi.object({
    jenis: Joi.string().required(),
  })
})


module.exports = {
  insertData,
  updateData
}