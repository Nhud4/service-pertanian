class Command {
  constructor(db) {
    /**
         * @typedef {import('../../../helpers/databases/mysql')} DB
         * @type {DB}
         */

    this.db = db
  }

  async insertData(document) {
    const sql = 'INSERT INTO tanaman (jenis) VALUES (?)'
    const result = await this.db.query(sql,  [document.jenis])
    return result
  }

  async updateData(document, params) {
    const sql = 'UPDATE tanaman SET ? WHERE id = ?'
    const values = [document, params.id]
    const result = await this.db.query({sql, values})
    return result[0]
  }

  async deleteData(params) {
    const sql = 'DELETE FROM tanaman WHERE id = ?'
    const values = [params.id]
    const result = await this.db.query({sql, values})
    return result[0]
  }
}

module.exports = Command