const Command = require('./command')
const Query = require('../queries/query')
const { isEmpty } = require('lodash')
const { UnprocessableEntityError, NotFoundError } = require('../../../helpers/utils/response')

class Domain {
  constructor(db) {
    this.command = new Command(db)
    this.query = new Query(db)
  }

  async insert(payload) {
    const {body} = payload

    const checkData = await this.query.getByType(body.jenis)
    if (!isEmpty(checkData)) throw new UnprocessableEntityError('type already exists')

    const document = {
      jenis: body.jenis.toLowerCase()
    }

    const insertData = await this.command.insertData(document)
    return insertData
  }

  async update(payload) {
    const {body, params} = payload
    
    const checkData = await this.query.getById(params.id)
    if (isEmpty(checkData)) throw new NotFoundError('data not found')

    const checkCode = await this.query.getByType(body.jenis)
    if (!isEmpty(checkCode) && checkCode.id !== params.id) {
      throw new UnprocessableEntityError('code already exists')
    }

    const document = {
      jenis: body.jenis.toLowerCase()
    }
    
    const insertData = await this.command.updateData(document, params)
    return insertData
  }

  async deleteData(payload) {
    const {params} = payload
    const checkData = await this.query.getById(params.id)
    if (isEmpty(checkData)) throw new NotFoundError('data not found')

    const remove = await this.command.deleteData(params)
    return remove
  }
}

module.exports = Domain