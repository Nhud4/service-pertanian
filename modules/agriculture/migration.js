const DB = require('../../helpers/databases/mysql')
const {mysqlConfig} = require('../../config/index')

const db = new DB(mysqlConfig)

const agriculture = async() => {
  const statement = {
    sql: `CREATE TABLE IF NOT EXISTS \`pertanian\` (
        \`id\` INT UNSIGNED NOT NULL AUTO_INCREMENT,
        \`tahun\` VARCHAR(255),
        \`kecId\` INT UNSIGNED,
        \`tanamanId\` INT UNSIGNED,
        \`tanam\` INT,
        \`panen\` INT,
        \`produksi\` INT,
        \`provitas\` VARCHAR(255),
        \`clusterId\` INT UNSIGNED,
        \`createdBy\` INT UNSIGNED,
        PRIMARY KEY (\`id\`),
        FOREIGN KEY (\`kecId\`) REFERENCES \`kecamatan\` (\`id\`) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (\`tanamanId\`) REFERENCES \`tanaman\` (\`id\`) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (\`clusterId\`) REFERENCES \`cluster\` (\`id\`) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (\`createdBy\`) REFERENCES \`user\` (\`id\`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`,
    values: []
  }
  await db.query(statement)
}

const iterasi = async() => {
  const statement = {
    sql: `CREATE TABLE IF NOT EXISTS \`iterasi\` (
        \`id\` INT UNSIGNED NOT NULL AUTO_INCREMENT,
        \`number\` INT,
        \`code\` VARCHAR(50),
        \`point\` INT UNSIGNED,
        \`date\` TIMESTAMP NULL,
        PRIMARY KEY (\`id\`),
        FOREIGN KEY (\`point\`) REFERENCES \`pertanian\` (\`id\`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`,
    values: []
  }
  await db.query(statement)
}

const init = async() => {
  await agriculture()
  await iterasi()
}

module.exports = init