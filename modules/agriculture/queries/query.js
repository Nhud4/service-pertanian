class Query {
  constructor (db) {
    /**
     * @typedef {import('../../../helpers/databases/mysql')} DB
     * @type {DB}
     */

    this.db = db
  }

  async listData(query) {
    let sql = `SELECT pertanian.id, tahun, kecamatan, jenis, tanam, panen, produksi, provitas, cluster, code
    FROM pertanian
    INNER JOIN kecamatan ON kecamatan.id = pertanian.kecId
    INNER JOIN tanaman ON tanaman.id = pertanian.tanamanId
    LEFT JOIN cluster ON cluster.id = pertanian.clusterId`
    let values = []

    if (query.clusterId) {
      sql += ' WHERE clusterId = ?'
      values.push(query.clusterId)
    }

    if (!query.clusterId && query.tanamanId) {
      sql += ' WHERE tanamanId = ?'
      values.push(query.tanamanId)
    }

    if (query.clusterId && query.tanamanId) {
      sql += ' AND tanamanId = ?'
      values.push(query.tanamanId)
    }

    const result = await this.db.query(sql, values)
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async allData() {
    const sql = 'SELECT * FROM pertanian'
    const result = await this.db.query(sql, [])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result
  }

  async getById(id) {
    const sql = 'SELECT * FROM pertanian WHERE id = ?'
    const result = await this.db.query(sql, [id])
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result[0]
  }

  async getByRegionAndType(kedId, tanamanId) {
    const sql = 'SELECT * FROM pertanian WHERE kecId = ? AND tanamanId = ?'
    const values = [kedId, tanamanId]
    const result = await this.db.query(sql, values)
      .then(([result]) => result)
      .then((result) => result.map(row => row))

    return result[0]
  }
}

module.exports = Query