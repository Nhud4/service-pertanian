const Query = require('./query')
const { isEmpty } = require('lodash')
const { NotFoundError } = require('../../../helpers/utils/response')

class Domain {
  constructor(db) {
    this.query = new Query(db)
  }

  async getList(payload) {
    const {query} = payload
    
    const getData = await this.query.listData(query)
    if (isEmpty(getData)) return []
    return getData
  }

  async getDetail(payload) {
    const getData = await this.query.getById(payload.id)
    if (isEmpty(getData)) throw new NotFoundError('data not found')
    return getData
  }
}

module.exports = Domain