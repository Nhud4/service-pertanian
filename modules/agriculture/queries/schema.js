const Joi = require('joi')

const detail = Joi.object({
  params: Joi.object({
    id: Joi.number().integer().required(),
  })
})

const listData = Joi.object({
  query: Joi.object({
    clusterId: Joi.number().integer().allow('', null).default(null),
    tanamanId: Joi.number().integer().allow('', null).default(null)
  })
})

module.exports = {
  detail,
  listData
}