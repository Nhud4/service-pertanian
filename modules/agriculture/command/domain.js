const Command = require('./command')
const Query = require('../queries/query')
const { isEmpty } = require('lodash')
const { UnprocessableEntityError, NotFoundError } = require('../../../helpers/utils/response')

class Domain {
  constructor(db) {
    this.command = new Command(db)
    this.query = new Query(db)
  }

  async insert(payload) {
    const {body, user} = payload

    const checkData = await this.query.getByRegionAndType(body.kecId, body.tanamanId)
    if (!isEmpty(checkData)) throw new UnprocessableEntityError('data already exists')

    const document = {
      tahun: body.tahun, 
      kecId: body.kecId,
      tanamanId: body.tanamanId, 
      tanam: body.tanam, 
      panen: body.panen, 
      produksi: body.produksi, 
      provitas: body.provitas, 
      createdBy: user.id
    }

    const insertData = await this.command.insertData(document)
    return insertData
  }

  async update(payload) {
    const {body, params, user} = payload
    
    const checkData = await this.query.getById(params.id)
    if (isEmpty(checkData)) throw new NotFoundError('data not found')

    const checkRegion = await this.query.getByRegionAndType(body.kecId, body.tanamanId)
    if (!isEmpty(checkRegion) && checkRegion.id !== params.id) {
      throw new UnprocessableEntityError('data already exists')
    }

    const document = {
      tahun: body.tahun, 
      kecId: body.kecId,
      tanamanId: body.tanamanId, 
      tanam: body.tanam, 
      panen: body.panen, 
      produksi: body.produksi, 
      provitas: body.provitas, 
      createdBy: user.id
    }
    
    const insertData = await this.command.updateData(document, params)
    return insertData
  }

  async delete(payload) {
    const {params} = payload
    const checkData = await this.query.getById(params.id)
    if (isEmpty(checkData)) throw new NotFoundError('data not found')

    const deleteData = await this.command.deleteData(params)
    return deleteData
  }
}

module.exports = Domain