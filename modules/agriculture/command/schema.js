const Joi = require('joi')

const insertData = Joi.object({
  body: Joi.object({
    tahun: Joi.string().required(), 
    kecId: Joi.number().integer().required(),
    tanamanId: Joi.number().integer().required(), 
    tanam: Joi.number().integer().required(), 
    panen: Joi.number().integer().required(), 
    produksi: Joi.number().integer().required(), 
    provitas: Joi.string().required()
  })
})

const updateData = Joi.object({
  params: Joi.object({
    id: Joi.number().integer().required()
  }),
  body: Joi.object({
    tahun: Joi.string().required(), 
    kecId: Joi.number().integer().required(),
    tanamanId: Joi.number().integer().required(), 
    tanam: Joi.number().integer().required(), 
    panen: Joi.number().integer().required(), 
    produksi: Joi.number().integer().required(), 
    provitas: Joi.string().required()
  })
})


module.exports = {
  insertData,
  updateData
}