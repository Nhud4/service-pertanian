class Command {
  constructor(db) {
    /**
         * @typedef {import('../../../helpers/databases/mysql')} DB
         * @type {DB}
         */

    this.db = db
  }

  async insertData(document) {
    const sql = `INSERT INTO pertanian (tahun, kecId, tanamanId, tanam, panen, produksi, provitas, createdBy) 
    VALUES (?, ?, ?, ?, ?, ?, ?, ?)`
    const values = [
      document.tahun, 
      document.kecId,
      document.tanamanId, 
      document.tanam, 
      document.panen, 
      document.produksi, 
      document.provitas, 
      document.createdBy
    ]
    const result = await this.db.query(sql,  values)
    return result
  }

  async updateData(document, params) {
    const sql = 'UPDATE pertanian SET ? WHERE id = ?'
    const values = [document, params.id]
    const result = await this.db.query({sql, values})
    return result[0]
  }

  async deleteData(params) {
    const sql = 'DELETE FROM pertanian WHERE id = ?'
    const values = [params.id]
    const result = await this.db.query({sql, values})
    return result[0]
  }
}

module.exports = Command