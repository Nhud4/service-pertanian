const Router = require('express-promise-router')
const {verifyBearerAuth} = require('../../middlewares/bearerAuth')
const queriesHandler = require('./queries/handler')
const commandHandler = require('./command/handler')
const {insertData, updateData} = require('./command/schema')
const {detail, listData} = require('./queries/schema')
const { validateSchema } = require('../../middlewares/index')

const router = Router()

router.get('/list', verifyBearerAuth, validateSchema(listData), queriesHandler.getList)
router.get('/detail/:id', verifyBearerAuth, validateSchema(detail), queriesHandler.getDetail)
router.post('/add', verifyBearerAuth, validateSchema(insertData), commandHandler.insertData)
router.put('/edit/:id', verifyBearerAuth, validateSchema(updateData), commandHandler.updateData)
router.delete('/delete/:id', verifyBearerAuth, validateSchema(detail), commandHandler.deleteData)

module.exports = router