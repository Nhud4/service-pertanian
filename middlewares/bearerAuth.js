const jwt = require('jsonwebtoken')
const getenv = require('getenv')
const {response, UnauthorizedError} = require('../helpers//utils/response')

const secretKey = getenv('BASIC_AUTH_PASSWORD', 'supersecret')

const verifyBearerAuth = (req, res, next) => {
  const auth = req.headers.authorization
  if (!auth) {
    return response(res, new UnauthorizedError('bearer auth is required'))
  }
  const [, token] = auth.split(' ')
  try {
    const decode = jwt.verify(token, secretKey)
    req.user = decode
    next()
  } catch (err) {
    return response(res, new UnauthorizedError('bearer auth is error'))
  }
}

module.exports = {
  verifyBearerAuth
}