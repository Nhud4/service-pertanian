const { response } = require('../helpers/utils/response')

const routeErrorHandler = (err, req, res, next) => {
  if (res.headerSent) {
    return next()
  }

  if (err) {
    return response(res, {
      code: err.code, message: err.message, data: err.data
    })
  }
  return response(res, { code: 500, message: err.message })
}

module.exports = {
  routeErrorHandler
}