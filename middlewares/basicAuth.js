const { basicAuth } = require('../config/index')
const { UnauthorizedError } = require('../helpers/utils/response')

const verifyBasicAuth = (req, res, next) => {
  const expectedAuth = Buffer.from(`${basicAuth.username}:${basicAuth.password}`).toString('base64')
  if (req.get('Authorization') !== `Basic ${expectedAuth}`) {
    throw new UnauthorizedError('Unauthorized.')
  }
  next()
}

module.exports = {
  verifyBasicAuth
}