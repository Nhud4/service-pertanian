const basicAuth = require('./basicAuth')
const validator = require('./validator')
const errorHandler = require('./errorHandler')

const validateSchema = validator.validateSchema
const verifyBasicAuth = basicAuth.verifyBasicAuth
const routeErrorHandler = errorHandler.routeErrorHandler

module.exports = {
  validateSchema,
  verifyBasicAuth,
  routeErrorHandler
}