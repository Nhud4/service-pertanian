// const {ValidationError} = require('joi')
const { UnprocessableEntityError } = require('../helpers/utils/response')

const validateSchema = (schema) =>
  async (req, res, next) => {
    try {
      req.validated = await schema.validateAsync(
        { query: req.query, body: req.body, params: req.params },
        { allowUnknown: true, stripUnknown: true }
      )
      next()
    } catch (err) {
      if (err) {
        throw new UnprocessableEntityError(
          'Failed to validate request.',
          err.details.map(({ path, message }) => ({ field: path[path.length - 1], message }))
        )
      }

      throw err
    }
  }

module.exports = {
  validateSchema
}